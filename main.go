package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/mux"
	"willnorris.com/go/microformats"
)

type JsonResourceDescriptor struct {
	Subject    string            `json:"subject,omitempty"`
	Aliases    []string          `json:"aliases,omitempty"`
	Properties map[string]string `json:"properties,omitempty"`
	Links      []Link            `json:"links,omitempty"`
}

type Link struct {
	Rel        string            `json:"rel,omitempty"`
	Type       string            `json:"type,omitempty"`
	Href       string            `json:"href,omitempty"`
	Titles     []string          `json:"titles,omitempty"`
	Properties map[string]string `json:"properties,omitempty"`
}

func mf2wf(w http.ResponseWriter, r *http.Request) {
	JRD := JsonResourceDescriptor{"", []string{}, make(map[string]string), []Link{}}
	uri, err := url.Parse(r.URL.Path[1:])
	if err != nil {
		return
	}
	JRD.Subject = uri.String()
	if uri.Scheme == "http" || uri.Scheme == "https" {
		resp, err := http.Get(uri.String())
		if err != nil {
			return
		}
		defer resp.Body.Close()
		data := microformats.Parse(resp.Body, uri)
		for k, v := range data.Rels {
			properties := make(map[string]string)
			if k == "http://tools.ietf.org/id/draft-dejong-remotestorage" {
				properties["http://tools.ietf.org/html/rfc6749#section-4.2"] = data.Rels["authorization_endpoint"][0]
			}
			link := Link{
				Rel:        k,
				Href:       v[0],
				Properties: properties,
			}
			JRD.Links = append(JRD.Links, link)
		}

	}
	j, _ := json.Marshal(JRD)
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	fmt.Fprintln(w, string(j))
}

func main() {
	r := mux.NewRouter()
	r = r.SkipClean(true)
	r.HandleFunc("/{uri:.*}", mf2wf)
	http.Handle("/", r)
	srv := &http.Server{
		Addr:         "0.0.0.0:8086",
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}
	log.Fatal(srv.ListenAndServe())
}
